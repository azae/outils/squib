
# Squib container

This is our [Squib](http://squib.rocks/) container used to build cards deck.

# Usage

use the `/workspace` volume to mount your tex directory and run :

      docker run --user `id -u`:`id -g` --rm -it -v $(pwd):/workspace registry.gitlab.com/azae/outils/squib rake
